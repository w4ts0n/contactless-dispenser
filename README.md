# Deutsch

## Was wird benötigt?

- Arduino Mini
- Status-LED
- Widerstände
- Ultraschallsensor „HC-SR04“
- Kabel für Verkabelung von Status-LED und HC-SR04
- viruzides Desinfektionsmittel
- Seife
- Schrauben
- Material zum Bau einer Halterung für Desinfektionsmittel oder Seife
- Barriere
- Programmcode

Optional:

- Zwei grüne LEDs
- Zwei gelbe LEDs
- Zwei rote LEDs

## Was ist Ziel des Projektes

Ein Sensor soll erkennen, dass sich etwas in einem vordefinierten Abstand befindet, eine Status-LED soll aktiviert werden und gleichwohl soll entweder Desinfektionsmittel oder Seife (je nachdem was in der Halterung eingehängt ist) von oben ausgegeben werden für eine Anzahl n an Sekunden.

## Sonstiges

Dieses Projekt ist maßgeblich inspiriert durch ein Projekt der deutschsprachigen Kreativschaffenden Achnina (https://invidious.snopyta.org/watch?v=RP5Xz8P5AcQ).

# Englisch

## What is needed?

- Arduino Mini
- Status LED
- resistors
- Ultrasonic sensor "HC-SR04
- Cable for wiring status LED and HC-SR04
- virucidal disinfectant
- soap
- screws
- Material to build a holder for disinfectant or soap
- Barrier
- Programme code

Optional:

- Two green LEDs
- Two yellow LEDs
- Two red LEDs

## What is the aim of the project

A sensor shall detect that something is in a predefined distance, a status LED shall be activated and at the same time either disinfectant or soap (depending on what is hung in the holder) shall be dispensed from the top for a number n of seconds.

Optionally, a fill level indicator would be displayed so that it is clear when the disinfectant or soap can be changed, implemented.

## Notes

This project is mainly inspired by a project by the German-speaking creative Achnina (https://invidious.snopyta.org/watch?v=RP5Xz8P5AcQ).
